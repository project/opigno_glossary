<?php

/**
 * @file
 * Contains all administration forms and callbacks
 */

 
/**
 * Custom term add form.
 * Adds a new term to the vocabulary, assigns it directly to the node and returns to
 * the node page, at the correct letter.
 */
function opigno_glossary_add_term_form($form, $form_state, $node) {
  module_load_include('inc', 'taxonomy', 'taxonomy.admin');
  
  $form = taxonomy_form_term($form, $form_state, array(), taxonomy_vocabulary_load(variable_get('opigno_glossary_vocabulary', 0)));
  
  $form['#node'] = $node;
  
  // Add submit callback on submit button
  $form['actions']['submit']['#submit'][] = 'opigno_glossary_add_term_form_submit';
  
  // Hide relations form
  $form['relations']['#attributes']['class'][] = 'element-hidden';
  
  return $form;
}

/**
 * Custom term add form submission callback
 */
function opigno_glossary_add_term_form_submit($form, &$form_state) {
  // Insert new term
  taxonomy_form_term_submit($form, $form_state);
  
  // Find last field delta
  $delta = db_select('field_data_' . OPIGNO_GLOSSARY_FIELD_NAME, 'fd')
              ->fields('fd', array('delta'))
              ->condition('fd.entity_id', $form['#node']->nid)
              ->condition('fd.revision_id', $form['#node']->vid)
              ->condition('fd.language', $form['#node']->language)
              ->orderBy('fd.delta', 'DESC')
              ->range(0, 1)
              ->execute()
              ->fetchField();
  
  // Add term to node
  db_insert('field_data_' . OPIGNO_GLOSSARY_FIELD_NAME)
    ->fields(array(
      'entity_type' => 'node',
      'bundle' => OPIGNO_GLOSSARY_NODE_TYPE,
      'deleted' => 0,
      'entity_id' => $form['#node']->nid,
      'revision_id' => $form['#node']->vid,
      'language' => $form['#node']->language,
      'delta' => $delta + 1,
      OPIGNO_GLOSSARY_FIELD_NAME . '_tid' => $form_state['tid']
    ))
    ->execute();
  
  // Redirect to the correct page
  $letter = strtoupper(substr($form_state['values']['name'], 0, 1));
  
  $form_state['redirect'] = url('node/' . $form['#node']->nid, array('query' => array('glossary_letter' => $letter))); // @todo remove previous glossary_letter GET param !
}

